/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

    const gCOMBO_SMALL = "Small";
    const gCOMBO_MEDIUM = "Medium";
    const gCOMBO_LARGE = "Large";
    const gPIZZA_HAWAI = "Hawai";
    const gPIZZA_SEAFOOD = "Seafood";
    const gPIZZA_BACON = "Bacon";  
    const gBASE_URL_DRINK = "http://203.171.20.210:8080/devcamp-pizza365/drinks";
    const gBASE_URL_VOUCHER = "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail";
    const vBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/orders";
    var gMenuCombo = "";
    var gOrderInfo = "";
    var gVoucherCode = "";
    var gNewOrderCreated = "";
    
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

$(document).ready(function(){

    //add event for Small Button
    $("#btn-small-click").click(onBtnSmallClick)

    //add event for Medium Button
    $("#btn-medium-click").click(onBtnMediumClick);

    //add event for Large Button
    $("#btn-large-click").click(onBtnLargeClick);

    //add event for Large Button
    $("#btn-seafood-pizza").click(onBtnSeaFoodClick);

    //add event for Large Button
    $("#btn-hawai-pizza").click(onBtnHawaiClick);

    //add event for Large Button
    $("#btn-bacon-pizza").click(onBtnBaconClick);

    callApiDrinkList();

    //add event for Send Button
    $("#btn-order-checking").on('click', onBtnSendClick);

    //add event for Confirmation Button
    $("#btn-order-create").click(callApiCreateNewOrder);
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */  

/*---------------MENU COMBO FUNCTIONS--------------------------------*/
    
    //function for Menu Combo object
    function getMenuCombo(paramMenu, paramDiameter, paramBakedRib, paramSalad, paramDrink, paramPrice){
        //create the menu object
        var vMenuComboObject = {
            menuName: paramMenu,
            diameter: paramDiameter,
            bakedRib: paramBakedRib,
            salad: paramSalad,
            drink: paramDrink,
            priceVND: paramPrice,

            displayMenuConsole(){
            console.log("Combo: " + this.menuName);
            console.log("Đường kính: " + this.diameter);
            console.log("Sườn nướng: " + this.bakedRib);
            console.log("Salad: " + this.salad);
            console.log("Đồ uống: " + this.drink);
            console.log("Giá: " + this.priceVND);
            }
        }
        gMenuCombo = vMenuComboObject;
        //console.log(gMenuCombo);
        return vMenuComboObject;
    }

    //function for Small Button
    function onBtnSmallClick(){
        console.log("%cSMALL-Combo-selected", "color:orange");
        changeMenuButtonColor(gCOMBO_SMALL);
        displayMenuConsoleClick(gCOMBO_SMALL);
    }

    //function for Medium Button
    function onBtnMediumClick(){
        console.log("%cMEDIUM-Combo-selected", "color:orange");
        changeMenuButtonColor(gCOMBO_MEDIUM);
        displayMenuConsoleClick(gCOMBO_MEDIUM);
    }

    //function for Small Button
    function onBtnLargeClick(){
    console.log("%cLARGE-Combo-selected", "color:orange");
    changeMenuButtonColor(gCOMBO_LARGE);
    displayMenuConsoleClick(gCOMBO_LARGE);
    }

/*---------------PIZZA TYPE FUNCTIONS--------------------------------*/

    //function for Hawai Pizza Button
    function onBtnHawaiClick(){
        changePizzaButtonColor(gPIZZA_HAWAI);
        displayPizzaConsoleClick(gPIZZA_HAWAI);
    }

    //function for Seafood Pizza Button
    function onBtnSeaFoodClick(){
        changePizzaButtonColor(gPIZZA_SEAFOOD);
        displayPizzaConsoleClick(gPIZZA_SEAFOOD);
    }

    //function for Bacon Pizza Button
    function onBtnBaconClick(){
        changePizzaButtonColor(gPIZZA_BACON);
        displayPizzaConsoleClick(gPIZZA_BACON);
    }

/*---------------ORDER SENDING--------------------------------*/

    //function for Send Button
    function onBtnSendClick(){

        //create order object
        var vOrderInfo = {
            selectedMenu: "",
            pizza: "",
            drinkCode: "",
            fullName: "",
            email: "",
            phone: "",
            address: "",
            message: "",
            voucher: "",
            percenDiscount: "",
    
            priceActualVND(){
              return (1 - this.percenDiscount/100) * gMenuCombo.priceVND
            }
        }
        gOrderInfo = vOrderInfo; 
        {  //to check combo and pizza buttons clicking
            
            var vSmallDataset = $("#btn-small-click").attr("data-is-selected-menu");
            var vMediumDataset = $("#btn-medium-click").attr("data-is-selected-menu");
            var vLargeDataset = $("#btn-large-click").attr("data-is-selected-menu");
    
            if ( vSmallDataset == "Y"){
                vOrderInfo.selectedMenu = "Small";
            }
            if ( vMediumDataset == "Y"){
                vOrderInfo.selectedMenu = "Medium";
            }
            if ( vLargeDataset == "Y"){
                vOrderInfo.selectedMenu = "Large";
            }
            if (vSmallDataset == "N" && vMediumDataset == "N" && vLargeDataset == "N"){
                alert("Bạn chưa chọn combo !!!");
                return false;
            }
    
            //to check pizza buttons clicking
                
            var vHawaiDataset = $("#btn-hawai-pizza").attr("data-is-selected-pizza");
            var vSeaFoodDataset = $("#btn-seafood-pizza").attr("data-is-selected-pizza");
            var vBaconDataset = $("#btn-bacon-pizza").attr("data-is-selected-pizza");
    
            if ( vHawaiDataset == "Y"){
                vOrderInfo.pizza = "Hawai";
                console.log("%cPIZZA-Type:", "color:orange", vOrderInfo.pizza);
            }
            if ( vSeaFoodDataset == "Y"){
                vOrderInfo.pizza = "SeaFood";
            }
            if ( vBaconDataset == "Y"){
                vOrderInfo.pizza = "Bacon";
            }
            if (vHawaiDataset == "N" && vSeaFoodDataset == "N" && vBaconDataset == "N"){
                alert("Bạn chưa chọn pizza !!!");
                return false;
            }
          }
        
        //read data from the web form
        readDataWeb(vOrderInfo);
    
        //validate the collected data
        var vValidResult = validateDataWeb(vOrderInfo);
            //console.log(vOrderInfo)
           
        //display the validated data on the web
        if (vValidResult == true){
            getVoucherCode();
            showDetailModal(vOrderInfo, gVoucherCode);
        };
    }


/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

//change the menu button's color
function changeMenuButtonColor(paramMenuColor){
    
    if (paramMenuColor === gCOMBO_SMALL){
        $("#btn-small-click").attr('class','btn btn-success w-100 font-weight-bold');
        $("#btn-small-click").attr('data-is-selected-menu', "Y");
        
        $("#btn-medium-click").attr('class', "btn btn-warning w-100 font-weight-bold");
        $("#btn-medium-click").attr("data-is-selected-menu", "N");
        
        $("#btn-large-click").attr('class', "btn btn-warning w-100 font-weight-bold");
        $("#btn-large-click").attr("data-is-selected-menu", "N");
    }
    else if (paramMenuColor === gCOMBO_MEDIUM){
        $("#btn-small-click").attr('class','btn btn-warning w-100 font-weight-bold');
        $("#btn-small-click").attr('data-is-selected-menu', "N");
        
        $("#btn-medium-click").attr('class', "btn btn-success w-100 font-weight-bold");
        $("#btn-medium-click").attr("data-is-selected-menu", "Y");
        
        $("#btn-large-click").attr('class', "btn btn-warning w-100 font-weight-bold");
        $("#btn-large-click").attr("data-is-selected-menu", "N");
    }
    else if (paramMenuColor === gCOMBO_LARGE){
        $("#btn-small-click").attr('class','btn btn-warning w-100 font-weight-bold');
        $("#btn-small-click").attr('data-is-selected-menu', "N");
        
        $("#btn-medium-click").attr('class', "btn btn-warning w-100 font-weight-bold");
        $("#btn-medium-click").attr("data-is-selected-menu", "N");
        
        $("#btn-large-click").attr('class', "btn btn-success w-100 font-weight-bold");
        $("#btn-large-click").attr("data-is-selected-menu", "Y");
    }
  }

//display the selected Menu Button to console.log
function displayMenuConsoleClick(paramMenu){
   
    if (paramMenu == "Small"){
      paramMenu = getMenuCombo("Small", 20, 2, 200, 2, 150000);
      return paramMenu.displayMenuConsole();
    }
    if (paramMenu == "Medium"){
      paramMenu = getMenuCombo("Medium", 25, 4, 300, 3, 200000);
      return paramMenu.displayMenuConsole();
    }
    if (paramMenu == "Large"){
      paramMenu = getMenuCombo("Large", 30, 8, 500, 4, 250000);
      return paramMenu.displayMenuConsole();
    }
  }

//change the pizza type button's color
function changePizzaButtonColor(paramPizzaColor){
    
    if (paramPizzaColor == gPIZZA_SEAFOOD){
        $("#btn-seafood-pizza").attr('class', "btn btn-success w-100 font-weight-bold");
        $("#btn-seafood-pizza").attr("data-is-selected-pizza", "Y");
      
        $("#btn-hawai-pizza").attr('class', "btn btn-warning w-100 font-weight-bold");
        $("#btn-hawai-pizza").attr("data-is-selected-pizza", "N");
      
        $("#btn-bacon-pizza").attr('class', "btn btn-warning w-100 font-weight-bold");
        $("#btn-bacon-pizza").attr("data-is-selected-pizza", "N");
    }
    else if (paramPizzaColor == gPIZZA_HAWAI){
        $("#btn-seafood-pizza").attr('class', "btn btn-warning w-100 font-weight-bold");
        $("#btn-seafood-pizza").attr("data-is-selected-pizza", "N");
      
        $("#btn-hawai-pizza").attr('class', "btn btn-success w-100 font-weight-bold");
        $("#btn-hawai-pizza").attr("data-is-selected-pizza", "Y");
      
        $("#btn-bacon-pizza").attr('class', "btn btn-warning w-100 font-weight-bold");
        $("#btn-bacon-pizza").attr("data-is-selected-pizza", "N");
    }
    else if (paramPizzaColor == gPIZZA_BACON){
        $("#btn-seafood-pizza").attr('class', "btn btn-warning w-100 font-weight-bold");
        $("#btn-seafood-pizza").attr("data-is-selected-pizza", "N");
      
        $("#btn-hawai-pizza").attr('class', "btn btn-warning w-100 font-weight-bold");
        $("#btn-hawai-pizza").attr("data-is-selected-pizza", "N");
      
        $("#btn-bacon-pizza").attr('class', "btn btn-success w-100 font-weight-bold");
        $("#btn-bacon-pizza").attr("data-is-selected-pizza", "Y");
    }
  }

//display the selected Pizza Type Button to console.log
function displayPizzaConsoleClick(paramPizza){
if (paramPizza == gPIZZA_HAWAI){
    console.log("%cPizza-HAWAI-selected", "color:orange");
}
if (paramPizza == gPIZZA_SEAFOOD){
    console.log("%cPizza-SEAFOOD-selected", "color:orange");
}
if (paramPizza == gPIZZA_BACON){
    console.log("%cPizza-BACON-selected", "color:orange");
}
}

//call API for loading the list of drink to the page
function callApiDrinkList(){
    
    $.ajax({
        url: gBASE_URL_DRINK,
        type: "GET",
        async: false,
        success: function(res){
            console.log(res);
            loadDataDrinkList(res)
        },
        error: function(ajacContext){
            console.log(ajacContext.status);
        }
    })
}

//load data to the drink's list
function loadDataDrinkList(paramDrink){
    //console.log(paramDrink);
    for (var index = 0; index < paramDrink.length; index++){
        var vDrinkSelect = $("#drink-select");
        var vNewOption = new Option ("Option Text", "Option Value");
            vNewOption.value = paramDrink[index].maNuocUong;
            vNewOption.text = paramDrink[index].tenNuocUong;
        vDrinkSelect.append(vNewOption);
    };
}
              
//read data from the web form
function readDataWeb(paramData){
    
    //add value collected from html elements to the order object
    paramData.fullName = $("#inp-fullname").val()
    paramData.email =  $("#inp-email").val();
    paramData.phone = $("#inp-phone").val();
    paramData.address = $("#inp-address").val();
    paramData.message = $("#inp-message").val();
    paramData.voucher = $("#inp-voucher-id").val();
    paramData.drinkCode = $("#drink-select").find(":selected").text();
     

    console.log(paramData.phone);
    
  }

//validate the collected data
function validateDataWeb(paramData){
    if (paramData.drinkCode == "Tất cả các loại nước uống"){
        alert("chưa chọn đồ uống !!!");
        return false;
    }
    if (paramData.fullName == ""){
        alert("chưa điền họ tên !!!");
        return false;
    }
    if (validateEmail(paramData.email) == false){
        return false;
    }
    if (paramData.phone == ""){
        alert("chưa điền số điện thoại !!!");
        return false;
    }
    if (isNaN(paramData.phone) == true){
        alert("số điện thoại phải điền dạng chữ số !!!");
        return false;
    }
    if (paramData.address == ""){
        alert("chưa điền địa chỉ !!!");
        return false;
    }
    return true;
}

//email checking
function validateEmail(paramEmail){
    if (paramEmail == ""){
      alert("chưa điền email !!!");
      return false
    }
    if (paramEmail.indexOf("@") === -1){
      alert("email phải có '@' !!!");
      return false;
    }
    if (paramEmail.startsWith("@") === true){
      alert("email phải có ký tự trước '@' !!!");
      return false;
    }
    if (paramEmail.endsWith("@") === true){
      alert("email phải có ký tự sau '@' !!!");
      return false;
    }
    if (paramEmail.indexOf(".") === -1){
      alert("email phải có dấu chấm !!!");
      return false;
    }
    return true
}

//function for getting voucher code
function getVoucherCode(){
    
    var vVoucherCode = gOrderInfo.voucher;
    console.log(vVoucherCode)

    $.ajax({
        url: gBASE_URL_VOUCHER + "/" + vVoucherCode,
        type: "GET",
        async: false,
        success: function(res){
            //console.log(res);
            gVoucherCode = res;
            //console.log(gVoucherCode);
            return gVoucherCode;          
        },
        error: function(ajaxContext){
            console.log(ajaxContext.status)
            console.log("Không tìm thấy voucher " + vVoucherCode);
            gVoucherCode = 0;
        }
    })
}

//function for Order Detail Modal
function showDetailModal(paramOrderObj, paramVoucher){

    console.log(paramVoucher);
  

    $("#order-modal").modal('show');
    //var getVoucherCode();
    //console.log(gVoucherCode)

    $("#modal-fullname").html(paramOrderObj.fullName);
    $("#modal-phone").html(paramOrderObj.phone);
    $("#modal-address").text(paramOrderObj.address);
    $("#modal-message").text(paramOrderObj.message);
    
    $("#modal-voucher").text(paramOrderObj.voucher);
    if (paramOrderObj.voucher != ""){
        if (paramVoucher.maVoucher == paramOrderObj.voucher){
            paramOrderObj.percenDiscount = paramVoucher.phanTramGiamGia;
        }
        if (paramVoucher == 0){
            paramOrderObj.percenDiscount = 0;
            alert("Voucher "+ paramOrderObj.voucher + " này không tồn tại" );
        }
    }
    if (paramOrderObj.voucher == ""){
        paramOrderObj.percenDiscount = 0;
    }
        
    $("#modal-detail-info").html("Xác nhận: " + paramOrderObj.fullName+ " , " + paramOrderObj.phone + " , " + paramOrderObj.address
    + '\n' + "Menu: " + gMenuCombo.menuName + " , sườn nướng " + gMenuCombo.bakedRib + " , nước " + gMenuCombo.drink + " ... "
    + '\n' + "Loại pizza: " + paramOrderObj.pizza + " , Giá: " + gMenuCombo.priceVND + " VND , Mã giảm giá: " + paramOrderObj.voucher
    + '\n' + "Phải thanh toán: " + paramOrderObj.priceActualVND()+ " VND (giảm giá " + paramOrderObj.percenDiscount + "% )"  
    );

}

//call API for creating new order
function callApiCreateNewOrder(){
    //set the new order object
    var vObjectRequest = {
        kichCo: gMenuCombo.menuName,
        duongKinh: gMenuCombo.diameter,
        suon: gMenuCombo.bakedRib,
        salad: gMenuCombo.salad,
        loaiPizza: gOrderInfo.pizza,
        idVourcher: gOrderInfo.voucher,
        idLoaiNuocUong: gOrderInfo.drinkCode ,
        soLuongNuoc: gMenuCombo.drink,
        hoTen: gOrderInfo.fullName,
        thanhTien: gOrderInfo.priceActualVND(),
        email: gOrderInfo.email,
        soDienThoai: gOrderInfo.phone,
        diaChi: gOrderInfo.address,
        loiNhan: gOrderInfo.message
    }

    $.ajax({
        url: vBASE_URL,
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(vObjectRequest),
        success: function(res){
            console.log(res);
            //gNewOrderCreated = res;
            showConfirmModal(res)
        },
        error: function(ajacContext){
            console.log(ajacContext.status);
        }
    })
}

//function for Confirmation Modal
function showConfirmModal(paramOrder){

    $("#confirm-modal").modal('show');
    $("#modal-confirm-ordercode").html(paramOrder.orderCode);
}